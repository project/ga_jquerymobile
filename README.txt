
Module: jQuery Mobile GA Plugin
Author: Jason Savino <http://drupal.org/user/411241>

Description
===========
Reporting of page hits using Google Analytics is wrong when using jQuery Mobile
if AJAX is enabled. This module adds an AJAX friendly script to your pages.


Requirements
============

* Google Analytics module (http://drupal.org/project/google_analytics)


Installation
============
* Copy the 'ga_jquerymobile' module directory into your Drupal
sites/all/modules directory as usual.


Usage
=====
There is no settings page for this module.
Use the Google Analytics setting page to enter your Google Analytics
account number.

Once the Google Analytics module in configured all appropriate pages 
will have the required JavaScript added to the HTML footer. You can
confirm this by viewing the page source from your browser.
